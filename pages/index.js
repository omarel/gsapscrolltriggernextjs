import { useEffect, useState, useRef } from 'react'
import Head from 'next/head'
import Image from 'next/image'
import Hero from '../components/Hero'
import Boxsection from '../components/homepage/Boxsection'
import BoxsectionSlider from '../components/homepage/BoxsectionSlider'
import Fullsection from '../components/homepage/Fullsection'
import Headerscripts from '../components/Headerscripts'
import Layout from '../components/Layout'

export default function Home() {
 


  const boxsection1copy = {
    headline: "Lorem ipsum dolor",
    label: "Residences",
    subline:"",
    link:"residences",
  }
  const fullsection1copy = {
    headline: "ILorem ipsum dolor",
    label: "Amenities",
    subline:"",
    link:"amenities",
  }
  const boxsection2copy = {
    headline: "Lorem ipsum dolor",
    label: "Neighborhood",
    subline:"",
    link:"neighborhood",
  }
  const fullsection2copy = {
    headline: "Lorem ipsum dolor",
    label: "Availability",
    subline:"",
    link:"availability",
  }

  return (
    <div>
      <Head> 
        <title></title>
        <meta name="description" content="" />
        <Headerscripts />
        <link rel="stylesheet" href="https://use.typekit.net/xjw3tlo.css"></link>
      </Head>

      <Layout>
      <Hero />
      <BoxsectionSlider copy={boxsection1copy} theimage='room2' />
      {/* <Boxsection copy={boxsection1copy} theimage='room2' /> */}
      <Fullsection copy={fullsection1copy} theimage='room3' />
      <Boxsection copy={boxsection2copy} theimage='room5' />
      <Fullsection copy={fullsection2copy} theimage='room4' />

      

      </Layout>

    </div>
  )
}
