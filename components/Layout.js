import { useEffect, useState, useRef } from 'react'
import Head from 'next/head'
import Header from './Header'
import Footer from './Footer'

export default function Layout(props) {
  
  const [scrolledDown, setscrolledDown] = useState(false)

  

  useEffect(() => {

    document.addEventListener("scroll", e => {
      let scrolled = document.scrollingElement.scrollTop;
      if (scrolled >= 20){
        setscrolledDown(true)
      } else {
        setscrolledDown(false)
      }
    })

  },[])

  return (
    <>
        <Header scrolleddown={scrolledDown} />
        <main>
            {props.children}
        </main>
        <Footer />
    </>
  )
}
