import { useEffect, useRef } from 'react'
import { Swiper, SwiperSlide} from 'swiper/react'
import { Navigation, EffectFade } from 'swiper'
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/effect-fade'
import Image from 'next/image'
import Link from 'next/link'
import styles from './boxsectionswiper.module.scss'
import { gsap } from "gsap"
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger); 

export default function Boxsection(props) {

    const theimage = props.theimage
    const headline = props.copy.headline
    const subline = props.copy.subline
    const label = props.copy.label
    const link = props.copy.link
    const boxSection = useRef()
    const swiperNavPrevRef = useRef(null)
    const swiperNavNextRef = useRef(null)

    useEffect(() => {
  
        const tl = gsap.timeline({
          repeat: 0, 
          scrollTrigger: {
              trigger: boxSection.current,
              start: "top bottom", 
              end: "+=500",
              scrub: 0.4,
          } 
        })
        tl.fromTo(boxSection.current, { 
          opacity:0,
          y:50,
          duration:1
            }, {
            opacity:1,
            y:0,
        })
        return () => {
            tl.scrollTrigger.kill();
          }
    },[]);

    return (
        <>
            <section className={`boxsectionslider ${styles.container}`} ref={boxSection}>
                <div className={`containerGlobal`}>

                    <div className={`${styles.holdphoto}`}>
                        {/* <Image src={`/images/${theimage}.jpg`} layout="fill" /> */}
                        <Swiper
                            modules={[Navigation, EffectFade]}
                            // spaceBetween={50}
                            // navigation
                            navigation={{
                                prevEl: swiperNavPrevRef.current,
                                nextEl: swiperNavNextRef.current,
                              }}
                            effect
                            // effect={'fade'}
                            speed={800}
                            slidesPerView={1}
                            onSlideChange={() => console.log('slide change')}
                            // onSwiper={(swiper) => console.log(swiper)}
                            className={styles.myswiper}
                            loop
                            // onBeforeInit={(swiper) => {
                            //     swiper.params.navigation.prevEl = swiperNavPrevRef.current;
                            //     swiper.params.navigation.nextEl = swiperNavNextRef.current;
                            // }}
                            onProgress={(swiper,progress) => {
                                console.log(swiper.params.effect)
                            }}
                            onInit={(swiper) => {
                                swiper.params.navigation.prevEl = swiperNavPrevRef.current;
                                swiper.params.navigation.nextEl = swiperNavNextRef.current;
                                swiper.navigation.init();
                                swiper.navigation.update();
                              }}
                            >
                            <SwiperSlide className={styles.swiperslide}><Image src={`/images/${theimage}.jpg`} layout="fill" /></SwiperSlide>
                            <SwiperSlide className={styles.swiperslide}><Image src={`/images/room4.jpg`} layout="fill" /></SwiperSlide>
                            <SwiperSlide className={styles.swiperslide}><Image src={`/images/room5.jpg`} layout="fill" /></SwiperSlide>
                            <div className={`${styles.swiperNav} ${styles.swiperNavPrev}`} ref={swiperNavPrevRef}>
                                <span></span>
                                <span></span>
                            </div>
                            <div className={`${styles.swiperNav} ${styles.swiperNavNext}`} ref={swiperNavNextRef}>
                                <span></span>
                                <span></span>
                            </div>
                        </Swiper>
                    </div>
                    
                    <div className={`${styles.copyarea}`}>
                        <div className={`${styles.col}`}>
                            <h3>{headline}</h3>
                            <p>{subline}</p>
                        </div>
                        <div className={`${styles.col}`}>
                            <Link href={`/${link}`}>
                                <a className="btn">{label}</a>
                            </Link>
                        </div>
                    </div>
                    

                </div>
            </section>
        </>
  )
}
