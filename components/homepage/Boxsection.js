import { useEffect, useRef } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import styles from './boxsection.module.scss'
import { gsap } from "gsap"
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger); 

export default function Boxsection(props) {

    const theimage = props.theimage
    const headline = props.copy.headline
    const subline = props.copy.subline
    const label = props.copy.label
    const link = props.copy.link
    const boxSection = useRef()


    useEffect(() => {
  
        const tl = gsap.timeline({
          repeat: 0, 
          scrollTrigger: {
              trigger: boxSection.current,
              start: "top bottom", 
              end: "+=500",
              scrub: 0.4,
          } 
        })
        tl.fromTo(boxSection.current, { 
          opacity:0,
          y:50,
          duration:1
            }, {
            opacity:1,
            y:0,
        })
        return () => {
            tl.scrollTrigger.kill();
          }
    },[]);

    return (
        <>
            <section className={`${styles.container}`} ref={boxSection}>
                <div className={`containerGlobal`}>
                    <div className={`${styles.holdphoto}`}>
                        <Image src={`/images/${theimage}.jpg`} layout="fill" />
                    </div>
                    <div className={`${styles.copyarea}`}>
                        <div className={`${styles.col}`}>
                            <h3>{headline}</h3>
                            <p>{subline}</p>
                        </div>
                        <div className={`${styles.col}`}>
                            <Link href={`/${link}`}>
                                <a className="btn">{label}</a>
                            </Link>
                        </div>
                    </div>
                    

                </div>
            </section>
        </>
  )
}
