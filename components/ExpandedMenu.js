import { useEffect, useRef, useState } from 'react'
import Link from 'next/link'
import styles from './ExpandedMenu.module.scss'
import { gsap } from "gsap"

export default function ExpandedMenu(props) {

  const menu = useRef()
  const tl = useRef()
  const toggle = props.toggle
      

  useEffect(() => {

    tl.current = gsap.timeline({ paused: true });
    tl.current.to(menu.current, { 
      top: 0,
      autoAlpha:1,
      duration:0.7,
      }, "+=0.2")

  },[])

  useEffect(() => {   

    toggle ? tl.current.play() : tl.current.reverse()
    // console.log(toggle)

  }, [toggle]);


  return (
    <>
      <section className={`${styles.expandedmenu}`} ref={menu}>

      </section> 
    </>
  )
}
