import { useEffect, useState, useRef } from 'react'
import Link from 'next/link'
import ExpandedMenu from './ExpandedMenu'
import styles from './Header.module.scss'
import { gsap } from "gsap"

export default function Header(props) {

  const headerBox = useRef()
  const scrolleddown = props.scrolleddown
  const [menuClicked, setmenuClicked] = useState(false)

  function handleMenuClicked() {
    setmenuClicked(!menuClicked)
    if (menuClicked === false) {
      document.body.classList.add("noScroll")
    } else {
      document.body.classList.remove("noScroll")
    }
  }


  useEffect(() => {
    
    // const boxes = [
    //   heroBox.current,
    //   heroBoxCopy.current,
    // ]
    const tl = gsap.timeline({
      repeat: 0, 
    })
    tl.from(headerBox.current, { 
      y: -40,
      opacity:0,
      duration:0.7,
      // delay:0.4
    }, "+=1")

  },[]);

  return (
    <>
      <ExpandedMenu toggle={menuClicked} />
      <header className={`${styles.header} ${scrolleddown ? styles['scrolleddown']: ''} ${menuClicked ? styles['menuclicked']: ''}`} ref={headerBox}>
        <div className={styles.container}>
          <div className={styles.incentive}>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div> 
          <div className={styles.holdcontainer}>
              
            <div className={styles.col}>
              <div className={`${styles.logoicon} ${scrolleddown ? styles['scrolleddown']: ''}`}>
              </div>
            </div> 
            <div className={`${styles.col} ${styles.rightside}`}>
              <div className={styles.links}>
                <Link href={'/'}>
                  <a className={styles.link}>Availability</a>
                </Link>
                <Link href={'/'}>
                  <a className={styles.link}>Contact</a>
                </Link>
              </div>
              <div className={`${styles.hamburgerIcon} ${scrolleddown ? styles['scrolleddown']: ''} ${menuClicked ? styles['menuclicked']: ''}`} onClick={handleMenuClicked}>
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div> 
          </div> 
        </div>
      </header> 
    </>
  )
}
