import { useEffect, useRef } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import Image from 'next/image'
import styles from './Hero.module.scss'
import { gsap } from "gsap"

export default function Hero(props) {

  const heroBox = useRef()
  const heroBoxCopy = useRef()
  const heroBoxCopy2 = useRef()
  const heroBoxCopy3 = useRef()


  useEffect(() => {
    
   
    const tl = gsap.timeline({
      repeat: 0, 
    })
    tl.from(heroBox.current, { 
      y: -40,
      opacity:0,
      duration:0.7,
    }, "+=0.4")
    tl.addLabel("startcopy")
    tl.from(heroBoxCopy.current, { 
      y: -40,
      opacity:0,
      duration:0.7,
    },"startcopy")
    tl.from(heroBoxCopy2.current, { 
      y: -40,
      opacity:0,
      duration:0.7,
    },"startcopy+=0.4")
    tl.from(heroBoxCopy3.current, { 
      y: 40,
      opacity:0,
      duration:0.7,
      // delay:0.4
    },"startcopy+=0.4")

  },[]);
  
  return (
    <>
      <section className={styles.hero} ref={heroBox}>
        <div className={styles.bgimage}>
            <Image src="/images/room1.jpg" layout='fill' />
        </div>
        <div className={`containerGlobal ${styles.herocontainer}`}>
          <h1 ref={heroBoxCopy}><span>Lorem Ipsum</span>
            Dolor.
          </h1>
          <h2 ref={heroBoxCopy2}><span>Lorem ipsum dolor </span>
          sit amet</h2>
          <div className={styles.buttonlockup} ref={heroBoxCopy3}>
            <Link href={'/'}>
              <a className={'btn'}>Lorem Ipsum</a>
            </Link>
          </div>
        </div>
        <div className={styles.downarrow}>
          <img src="/images/icons/downarrow.svg" alt="Down Arrow" />
        </div>
      </section> 
    </>
  )
}
